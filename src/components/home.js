import React from 'react';
import User from '../models/user';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      model: new User()
    };
  }

  componentDidMount() {
    let xhr = this.state.model.fetch();
    xhr.done(() => {
      this.setState({model: this.state.model});
    });
  }

  render() {
    return <div className='home'>
      <h1>Hello World</h1>
      <h2>From server</h2>
      <p>Name: {this.state.model.get('name')}</p>
      <p>Age: {this.state.model.get('age')}</p>
    </div>;
  }
}
