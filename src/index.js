import $ from 'jquery';
import Backbone from 'backbone';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Router from './router';
import registerServiceWorker from './registerServiceWorker';

window.appRouter = new Router();

$(document).ready(() => {
  registerServiceWorker();
  Backbone.history.start();
  console.debug(process.env);
});
