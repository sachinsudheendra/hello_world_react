import Backbone from 'backbone';

export default class User extends Backbone.Model {
  defaults() {
    return {
      name: '',
      age: 0
    }
  }

  url() {
    return 'http://private-1348d-person18.apiary-mock.com/user';
  }
}
