import React from 'react';
import ReactDOM from 'react-dom';
import Backbone from 'backbone';
import Header from './components/header';
import Home from './components/home';


export default class Router extends Backbone.Router {
  routes() {
    return {
      'home': 'home',
      '.*': 'home'
    }
  }

  home() {
    this.navigate('home');
    this._renderHeader(<Header />);
    this._renderContent(<Home />);
  }

  _renderContent(component) {
    let container = document.getElementById('root');
    ReactDOM.unmountComponentAtNode(container);
    ReactDOM.render(component, container);
  }

  _renderHeader(component) {
    let container = document.getElementById('header');
    ReactDOM.unmountComponentAtNode(container);
    ReactDOM.render(component, container);
  }
}
